import React, { Component } from "react";
export default class ContactUs extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <section id="contact">
        <div className="row section-head">
          <div className="ten columns">
            <p className="lead">
              Feel free to contact me for any work or suggestions below
            </p>
          </div>
        </div>
        <div className="row">
          <aside className="eigth columns footer-widgets">
            <div className="widget">
              <h4>
                <i className="fa fa-facebook-square" aria-hidden="true" />
                &nbsp; Facebook: &nbsp;
                <a
                  href={resumeData.linkedinId}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  Thanh Hieu
                </a>
              </h4>
              <h4>
                <i className="fa fa-envelope" aria-hidden="true" />
                &nbsp; Email: &nbsp;
                <a href="mailto:hieulethanh243@gmail.com?subject = Feedback&body = Message">
                  Send Feedback
                </a>
              </h4>
            </div>
          </aside>
        </div>
      </section>
    );
  }
}
