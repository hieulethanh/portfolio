import React, { Component } from "react";

export default class About extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <section id="about">
        <div className="row">
          <div className="three columns">
            <img className="profile-pic" src="images/profilepic.jpg" alt="" />
          </div>

          <div className="nine columns main-col">
            <h2>About Me</h2>
            <p>{resumeData.aboutme}</p>

            <div className="row">
              <div className="columns contact-details">
                <h2>Contact Details</h2>
                <p className="address">
                  <span>
                    <i class="fa fa-user" aria-hidden="true" />
                    &nbsp;
                    {resumeData.name}
                  </span>
                  <br />
                  <span>
                    <i class="fa fa-flag" />
                    &nbsp;
                    {resumeData.address}
                  </span>
                  <br />
                  <span>
                    <i className="fa fa-facebook-square" aria-hidden="true" />
                    &nbsp;
                    <a
                      href={resumeData.website}
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      Thanh Hieu
                    </a>
                  </span>
                  <br />
                  <span>
                    <i className="fab fa-r-project" />
                    &nbsp;
                    <a
                      href="https://quiz-answer.netlify.app"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      Quiz Answer
                    </a>{" "}
                    (My project)
                  </span>
                  <br />
                  <span>
                    <i className="fab fa-r-project" />
                    &nbsp;
                    <a
                      href="https://hiquishop.netlify.app/"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      Shop quần áo
                    </a>{" "}
                    (My project)
                  </span>
                  <br />
                  <span>
                    <i className="fab fa-r-project" />
                    &nbsp;
                    <a
                      href="https://herenow.co/"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      Check-in hotel
                    </a>{" "}
                    (My project)
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
