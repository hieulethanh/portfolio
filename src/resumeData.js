let resumeData = {
  imagebaseurl: "https://rbhatia46.github.io/",
  name: "Lê Thanh Hiếu",
  role: "Fullstack Web Developer",
  linkedinId: "https://www.facebook.com/quickkiemsy/",
  skypeid: "Your skypeid",
  roleDescription:
    " I like dabbling in various parts of frontend + backend development and like to learn about new technologies, write technical articles or simply play games in my free time.",
  socialLinks: [
    {
      name: "facebook",
      url: "https://www.facebook.com/quickkiemsy/",
      className: "fa fa-facebook",
    },
    {
      name: "gitlab",
      url: "https://gitlab.com/hieulethanh",
      className: "fa fa-github",
    },
    {
      name: "instagram",
      url: "https://www.instagram.com/imthanhieu/",
      className: "fa fa-instagram",
    },
  ],
  aboutme:
    "I am currently a pre-final year student at HCMC University of Technology and pursuing my dream from here. I am a self taught FullStack Web Developer, having worked for one year. I believe that to be successful in life, one needs to be obsessive with their dreams and keep working towards them.",
  address: "VietNam",
  website: "https://www.facebook.com/quickkiemsy/",
  education: [
    {
      UniversityName: "Ho Chi Minh City University of Technology",
      specialization: "Computer Science",
      MonthOfPassing: "Aug 2016 - ",
      YearOfPassing: "Now",
      Achievements: "A lot of assignments",
    },
  ],
  work: [
    {
      CompanyName: "MIdeas CO.,JSC",
      specialization: "Intership & Frontend Web Developer",
      MonthOfLeaving: "Apr 2019 - ",
      YearOfLeaving: "Aug 2019",
      Achievements: "Front-end Developer for Product of The Company",
      companyImg: "images/portfolio/mideas.jpeg",
    },
    {
      CompanyName: "DayOne Teams",
      specialization: "Front-end & Back-end Developer",
      MonthOfLeaving: "Sept 2019 - ",
      YearOfLeaving: "Oct 2020",
      Achievements:
        "Involving in projects of The company: Supply Hive, HereNow, MyPillChart,...",
      companyImg: "images/portfolio/dayone.svg",
    },
  ],
  skillsDescription: "Programing Language",
  skills: [
    {
      skillname: "HTML5",
    },
    {
      skillname: "CSS",
    },
    {
      skillname: "Nodejs",
    },
    {
      skillname: "Typescript",
    },
    {
      skillname: "Javascript",
    },
  ],
  skillFrame: "Framework",
  frames: [
    {
      frame: "ReactJS",
    },
    {
      frame: "Loopback4",
    },
    {
      frame: "Express",
    },
  ],
  design: "UX/UI Designer",
  tools: [
    {
      tool: "Figma",
    },
    {
      tool: "Sketch",
    },
    {
      tool: "Photoshop",
    },
    {
      tool: "Illustrator",
    },
  ],
  portfolio: [
    {
      name: "Hana.ai",
      description: "Chatbot",
      imgurl: "images/portfolio/hana.png",
    },
    {
      name: "MyPillChart",
      description: "Web, Mobile App",
      imgurl: "images/portfolio/mypill.png",
    },
    {
      name: "Herenow",
      description: "Hotel Checkin app",
      imgurl: "images/portfolio/herenow.svg",
    },
    {
      name: "Supply Hive",
      description: "Web App",
      imgurl: "images/portfolio/supply-hive.svg",
    },
    {
      name: "DayOne Company Website",
      description: "Landing Page",
      imgurl: "images/portfolio/dayone.svg",
    },
  ],
  testimonials: [
    {
      description: "Photoshop",
      img: "images/pts.png",
    },
    {
      description: "Illustrator",
      img: "images/ai.png",
    },
    {
      description: "Figma",
      img: "images/figma.png",
    },
  ],
};

export default resumeData;
